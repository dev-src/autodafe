= autodafe news

Repository head::
   configure creates .deps when required (e,g. LIBTOOL is in use).

1.0: 2024-06-01::
   makemake appends a tests.mk if it finds one.
   The deconfig patch output deletes config.h if there are no residuals.
   Added REQUIRED keyword.
   Replaced CHECK_STRUCT and CHECK_UNION with CHECK_MEMBERS.

0.8: 2024-05-18::
   configure accepts make variable settings on the commmand line.
   Full support for out-of-directory configure and build.
   Added configure --build option.
   Added makemake -x "" to nuke all autotools boilerplate except "all".
   Don't use .deps, all dependebcies are now explicit in the Makefile.

0.7: 2024-05-08::
   Added CHECK_SCRIPT for build extension scripting.
   makemake -e supports expabding out cluttery variablw definitions.
   Replace PACKAGE_NAME with PACKAGE and PACKAVE_VERION with VERSION.
   Many improvemenys to documentation.

0.6: 2024-05-04::
   Unneeded config.h headers are automatically stripped out.
   Added CHECK_UNION, CHECK_CONFIG and CHECK_PROGRAM to configure.
   configure now accepts CC/CFLAGS/LDFLAGS, etc. from environment.

0.5: 2024-04-30::
   Added "configure" tool.
   De-obfuscated makefiles no longer have a libtool dependency.
   Asdded deconfig -s option. 

0.4: 2024-04-26::
   Improved removal of stub productions.
   config.mk.in generation for residual symbols.

0.3: 2024-04-20::
   Significant expansion and revision of the HOWTO.
   makemake -c knows about more autotools debris to clean up.
   deconfig, for patching out obsolete HAVE_ sections, now exists.

0.2: 2024-04-16::
   Improvements to the HOWTO.
   Remove many more random bits of autoconf debris.
   The -x option takes a regular expression now.
   Warnings are issued on potential portability issues.
   Errors and warnings report locations in GCC format.
   Support testing makemake on an unaltered directory

0.1: 2024-04-06::
   Initial alpha release.
