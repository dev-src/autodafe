#! /bin/sh
## Test --srcdir case of configure
#
# vpath.sh [-d] [-p options] [-s phase] [-v]
#
# The test harness for makemake.  The default behavior is to
# cd into the directory ${project}-project and test makemake
# there.  The project is configured and built, then makemake
# is applied to it and the build done with the dobfuscated makefile.
#
# Options:
#    -d: dryrun, echo commands but don't run them
#    -p: pass options to makemake
#    -s: stop after specified phase
#    -v: don't suppress command and message output
#
# Output is in TAP.

# shellcheck disable=SC1091
. ./common-setup.sh

msgsink=/dev/null
stopafter="end"
run=""
makeopts=""
pass_opts=""
while getopts dp:s:v opt
do
    case $opt in
	d) run="echo"; msgsink=/dev/stdout;;
	p) pass_opts="${pass_opts} $OPTARG";;
	s) stopafter=$OPTARG;;
	v) msgsink=/dev/stdout;;
	*) echo "$0: unknown flag $opt" >&2; exit 1;;
    esac
done
# shellcheck disable=SC2004
shift $(($OPTIND - 1))

if [ "$stopafter" != "" ]
then
    case "$stopafter" in
	configure|deconfig|generation|patching|makemake|make|end) ;;
	*) echo "$0: unknown stop phase $stopafter"; exit 1 ;;
    esac
fi

if [ "$stopafter" != "end" ]
then
    builddir="$(pwd)/testbuild"
    msgsink=/dev/stdout
else
    builddir="/tmp/${stem}$$"
    trap 'rm -fr $builddir' EXIT HUP INT QUIT TERM
fi

here=$(pwd)

# In theory we could do this with giflib or sandbox
stem=sandbox

rm -fr "$builddir"

mkdir "${builddir}"

# shellcheck disable=SC2086
$run cp -r ${stem}-project ${builddir}/src >"${msgsink}" 2>&1
tapout "${stem} vpath directory copy"

# shellcheck disable=SC2086
$run tapcd "${builddir}/src"

# shellcheck disable=SC2086
$run autoreconf --install  >"${msgsink}" 2>&1
tapout "${stem} vpath autoreconf"

if [ "$stopafter" = "autoreconf" ]
then
    exit 0
fi

printf "\n*** About to configure...\n" >"${msgsink}" 2>&1
# shellcheck disable=SC2086
$run ./configure >"${msgsink}" 2>&1
tapout "${stem} vpath configure"

if [ "$stopafter" = "configure" ]
then
    exit 0
fi

printf "\n*** About to generate the patch...\n" >"${msgsink}" 
# shellcheck disable=SC2086
$run deconfig >lift.patch 2>"${msgsink}"

if [ "$stopafter" = "generation" ]
then
    exit 0
fi

printf "\n*** About to patch with deconfig output...\n" >"${msgsink}" 
# shellcheck disable=SC2086
$run patch -p1 <lift.patch >"${msgsink}" 2>&1

if [ "$stopafter" = "patching" ]
then
    exit 0
fi

# Forse regeneration of config.mk include
rm config.h

printf "\n*** About to 'makemake %s .'...\n" "$pass_opts" >"${msgsink}" 2>&1
# shellcheck disable=SC2086
$run makemake $pass_opts . >"${msgsink}" 2>&1
tapout "${stem} vpath makemake ${pass_opts}"

if [ "$stopafter" = "makemake" ]
then
    exit 0
fi

mkdir "${builddir}/build"

# shellcheck disable=SC2086
$run tapcd "${builddir}/build"

printf "\n*** About to configure...\n" >"${msgsink}" 2>&1
# shellcheck disable=SC2086
$run ${here}/../configure --srcdir=../src >"${msgsink}" 2>&1
tapout "${stem} vpath configure"

if [ "$stopafter" = "configure" ]
then
    exit 0
fi

printf "\n*** Dumping configuration...\n" >"${msgsink}" 2>&1
$run cat config.mk >"${msgsink}" 2>&1

if [ "$stopafter" = "dump" ]
then
    exit 0
fi

printf "\n*** About to make...\n" >"${msgsink}" 2>&1
# shellcheck disable=SC2086
$run make ${makeopts} >"${msgsink}" 2>&1
tapout "${stem} vpath make"

#if [ "$stopafter" = "make" ]
#then
#    exit 0
#fi

printf "\n*** Done.\n" >"${msgsink}" 2>&1

echo "ok - ${stem}: test of VPATH build succeeded."

if [ "$stopafter" != "end" ] && [ "$run" = "" ]
then
   echo "${stem}: build results left in ${builddir}"
fi

# end

