## Test -lmode translation of linker flags

#options: -l -q

# This input reveals a bug.  The -avoid-version line
# gets clobbered, leaving a dangling \.  The obvious way
# to fix this, by having clearout remove dangling
# continuations, damages myltine production bodies with
# continuations.

DYNMOD_LD_FLAGS = \
  -module           \
  -no-undefined     \
  $(XSTATIC)        \
  -export-dynamic   \
  -avoid-version
foo:
	$(CC) foo.c $(DYNMOD_LD_FLAGS)
