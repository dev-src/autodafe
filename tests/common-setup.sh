#!/bin/sh
# Boilerplate code begins.
#
# SPDX-FileCopyrightText: Copyright Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: BSD-2-Clause

# Not all platforms have a 'realpath' command, so fake one up if needed
# using $PWD.
# Note: GitLab's CI environment does not seem to define $PWD, however, it
# does have 'realpath', so use of $PWD here does no harm.
unset CDPATH	# See https://bosker.wordpress.com/2012/02/12/bash-scripters-beware-of-the-cdpath/

command -v realpath >/dev/null 2>&1 ||
    realpath() { test -z "${1%%/*}" && echo "$1" || echo "$PWD/${1#./}"; }

# Necessary so we can see reposurgeon and repocutter
PATH=$(realpath ..):$(realpath .):${PATH}

tapcd () {
    cd "$1" >/dev/null || ( echo "not ok: $0: cd failed"; exit 1 )
}

tapout () {
    if [ "$?" != 0 ]
    then
        echo "not ok - $1";
	exit 0
    fi
}

# Produce a TAP line from the return status of the last command
# executed.  Does not start a new log stanza. First argument is the
# group label. Second is the command return status to dispatch on, 0
# for success and any other value for failure. Third is the legend
# that later functions should use to report success or failure.
# Fourth, if given, is the name of a capture of the command's stdout
# and stderr.
#
# If the command return is nonzero, the captured output has been
# passed, and the size of the capture is nonzero, the diff is dumped
# as a YAML attachment conforming to TAP.  Beginning the command with "!"
# allows an expected failure status to be processed returning success.
#
# The -d option forces the capture to be dumped even on command success.
# The -e option reports failure if the capture is nonempty.
tapcheck() {
    status="$?"
    debug="no"
    need_empty=""
    while getopts :de opt
    do
	case $opt in
	    d) debug=yes;;
	    e) need_empty="-e";;
	    *) echo "not ok - unknown option in tapcheck"; exit 1;;
	esac
    done
    # shellcheck disable=SC2004
    shift $(($OPTIND - 1))
    legend="$1"
    outfile="$2"
    case "${status}" in
	0) ;;
	*) echo "not ok - ${legend} failed";
	   if [ -n "${outfile}" ]
	   then
	       echo "  --- |"
	       echo "  Return status ${status}"
	       sed <"${outfile}" -e 's/^/  /'
	       echo "  ..."
	   fi
	   exit 1
	   ;;
    esac

    if [ "${need_empty}" = "-e" ] && [ -n "${outfile}" ] && [ -s "${outfile}" ]
    then
	echo "not ok - ${legend} had unexpected nonempty output.";
	echo "  --- |"
	echo "  Return status ${status}"
	sed <"${outfile}" -e 's/^/  /'
	echo "  ..."
	exit 1
    fi
    
    echo "ok - ${legend} succeeded"
    if [ "${debug}" = "yes" ] && [ -n "${outfile}" ] && [ -s "${outfile}" ]
    then
	echo "  --- |"
	echo "  Return status ${status}"
	sed <"${outfile}" -e 's/^/  /'
	echo "  ..."
    fi
}

